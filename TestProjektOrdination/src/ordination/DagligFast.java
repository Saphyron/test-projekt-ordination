package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel,
			double morgenAntal, double middagAntal, double aftenAntal, double natAntal)  {
		super(startDen, slutDen);
		
		if (morgenAntal + middagAntal + aftenAntal + natAntal > 4) {
			throw new IllegalArgumentException("Samlet dosis er over 4");
		}
		if (morgenAntal < 0) {
			throw new IllegalArgumentException("Morgen dosis er negativ");
		} 
		if (middagAntal < 0) {
			throw new IllegalArgumentException("Morgen dosis er negativ");
		} 
		if (aftenAntal < 0) {	
			throw new IllegalArgumentException("Aften dosis er negativ");
		} 
		if (natAntal < 0) {
			throw new IllegalArgumentException("Nat dosis er negativ");
		}		
		setLaegemiddel(laegemiddel);
		doser = new Dosis[] { new Dosis(LocalTime.of(8, 0), morgenAntal), new Dosis(LocalTime.of(11, 0), middagAntal), 
			new Dosis(LocalTime.of(17, 0), aftenAntal) , new Dosis(LocalTime.of(23, 0), natAntal)};
	}

	@Override
	public double samletDosis() {
		double sum = 0;
		for (Dosis dose : doser) {
			sum += dose.getAntal();
		}
		return sum;
	}

	@Override
	public double doegnDosis() {
		return (double) samletDosis() / super.antalDage();
	}

	@Override
	public String getType() {
		return "Daglig Fast";
	}

	public void createDosis(Dosis dosis) {
		for (int i = 0; i < doser.length; i++) {
			if (doser[i] != null) {
				doser[i] = dosis;
				return;
			}
		}
	}

	public Dosis[] getDoser() {
		return doser;
	}
	
	public void removeDosis(Dosis dosis) {
		// Liniær søgning
		for (int i = 0; i < doser.length; i++) {
			if (doser[i].equals(dosis)) {
				doser[i] = null;
				return;
			}
		}
		System.err.println("Den ønskede dosis som skal fjernes findes ikke");
	}

}
